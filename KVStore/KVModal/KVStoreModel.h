//
//  KVStoreModal.h
//  KVStore
//
//  Created by Prashant Pandey on 13/07/16.
//  Copyright © 2016 Prashant Pandey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMDatabaseQueue.h"

@interface KVItem : NSObject

@property (strong, nonatomic) NSString *itemId;
@property (strong, nonatomic) id itemObject;
@property (strong, nonatomic) NSDate *createdTime;

@end

@interface KVStoreModel : NSObject

- (id) initDataBaseWithName:(NSString *)dbName;
- (void) createTableWithName:(NSString *)tableName;
- (BOOL) isTableExistsWithName:(NSString *)tableName;
- (void) clearTableWithName:(NSString *)tableName;
- (void) closeDataBase;

#pragma mark - 
#pragma mark -  Get and Insert Methods 

- (void)insertObject:(id)object withId:(NSString *)objectId intoTable:(NSString *)tableName;
- (id)getObjectById:(NSString *)objectId fromTable:(NSString *)tableName;

- (KVItem *)getKeyValueItemById:(NSString *)objectId fromTable:(NSString *)tableName;

- (void)insertString:(NSString *)str withId:(NSString *)stringId intoTable:(NSString *)tableName;
- (NSString *)getStringById:(NSString *)strId fromTable:(NSString *)tableName;

- (void)insertNumber:(NSNumber *)number withId:(NSString *)numberId intoTable:(NSString *)tableName;
- (NSNumber *)getNumberById:(NSString *)numberId fromTable:(NSString *)tableName;

- (NSArray *)getAllItemsFromTable:(NSString *)tableName;
- (NSUInteger)getCountFromTable:(NSString *)tableName;

- (void)deleteObjectById:(NSString *)objectId fromTable:(NSString *)tableName;
- (void)deleteObjectsByIdArray:(NSArray *)objectIdArray fromTable:(NSString *)tableName;
- (void)deleteObjectsByIdPrefix:(NSString *)objectIdPrefix fromTable:(NSString *)tableName;

@end
