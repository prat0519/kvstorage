//
//  ViewController.h
//  KVStore
//
//  Created by Prashant Pandey on 11/07/16.
//  Copyright © 2016 Prashant Pandey. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BookDetailViewController ;

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtFieldBookID;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldBookTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldBookAuthor;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldInputKey;

@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnClear;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

- (IBAction)btnActionSaveObjectToDB:(id)sender;
- (IBAction)btnActionClearFields:(id)sender;

@end

