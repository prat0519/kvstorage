//
//  BookDetailViewController.h
//  KVStore
//
//  Created by Prashant Pandey on 18/07/16.
//  Copyright © 2016 Prashant Pandey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblBookTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBookAuthor;
@property (weak, nonatomic) IBOutlet UILabel *lblBookID;

@property (nonatomic , strong) NSDictionary *dictSelectedBookDetails ;

@end
