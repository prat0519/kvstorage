//
//  ViewController.m
//  KVStore
//
//  Created by Prashant Pandey on 11/07/16.
//  Copyright © 2016 Prashant Pandey. All rights reserved.
//

#import "ViewController.h"
#import "KVStoreModel.h"
#import "BookDetailViewController.h"


@interface ViewController ()
{
    NSDictionary *queryBookDetails ;
    KVStoreModel *store ;
}

@end

@implementation ViewController
@synthesize txtFieldBookID, txtFieldBookTitle, txtFieldBookAuthor, txtFieldInputKey , btnNext, btnSave, btnClear ;

static NSString *tableName = @"book_table";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //Method to calculate multiples of any number in reverse order...
    [self findMultipleOfGivenNumber:7];

    btnSave.layer.cornerRadius = 15.0f ;
    btnClear.layer.cornerRadius = 15.0f ;
    btnNext.layer.cornerRadius = 15.0f ;
    
    //KVStorage initialization
    store = [[KVStoreModel alloc] initDataBaseWithName:@"bookDB.db"];
    [store createTableWithName:tableName];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Custom Method

- (void)findMultipleOfGivenNumber:(NSUInteger)number
{
    int remainder = 300 / number ;
    NSMutableArray *mArray = [[NSMutableArray alloc] init] ;
    for (int i = remainder ; i >= 1 ; i--) {
        NSLog(@"%lu", (unsigned long)remainder) ;
        NSNumber *result = [NSNumber numberWithUnsignedInteger:(number * i)]  ;
        [mArray addObject:result] ;
    }
}

-(void)clearOutletFields
{
    txtFieldBookID.text = @"" ;
    txtFieldBookTitle.text = @"" ;
    txtFieldBookAuthor.text = @"" ;
    txtFieldInputKey.text = @"" ;
    
}


#pragma mark -
#pragma mark - Button Action

- (IBAction)btnActionSaveObjectToDB:(id)sender {
    
    if ([txtFieldBookID.text length] == 0 && [txtFieldBookTitle.text length] == 0 && [txtFieldBookAuthor.text length] == 0) {
        [self showAlertWithTitle:@"Alert" andMessage:@"Please enter value in all reamining fields"];
    }
    else{
        NSString *key = txtFieldBookID.text ;
        NSDictionary *user = @{@"Id": txtFieldBookID.text, @"Title": txtFieldBookTitle.text, @"Author": txtFieldBookAuthor.text};
        [store insertObject:user withId:key intoTable:tableName];
        [self showAlertWithTitle:@"Success" andMessage:@"Insert query completed successfully"];
    }
}



- (IBAction)btnActionClearFields:(id)sender {
    
    [self clearOutletFields] ;

}

#pragma mark -
#pragma mark - UIAlertAction Method

-(void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                               }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark -
#pragma mark - UIStoryboardSegue Method

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowBookDetails"]) {
        NSString *key = txtFieldInputKey.text ;
        queryBookDetails = [store getObjectById:key fromTable:tableName];
        NSLog(@"query data result: %@", queryBookDetails);
        if(nil != queryBookDetails && [queryBookDetails isKindOfClass:[NSDictionary class]] && (queryBookDetails).count > 0)
        {
            BookDetailViewController *bookDetailsVC = (BookDetailViewController *)[segue destinationViewController];
            bookDetailsVC.dictSelectedBookDetails = queryBookDetails ;

        }
        else
        {
            [self showAlertWithTitle:@"Alert" andMessage:@"Please enter the valid ID to find the details"];

        }
       
    }
   
}

#pragma mark -
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldDidEndEditing:(UITextField *)textField
{
    return YES ;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.txtFieldInputKey resignFirstResponder];
    [self.txtFieldBookID resignFirstResponder];
    
}
@end
