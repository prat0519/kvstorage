//
//  BookDetailViewController.m
//  KVStore
//
//  Created by Prashant Pandey on 18/07/16.
//  Copyright © 2016 Prashant Pandey. All rights reserved.
//

#import "BookDetailViewController.h"

@interface BookDetailViewController ()

@end

@implementation BookDetailViewController
@synthesize lblBookID, lblBookTitle, lblBookAuthor ,dictSelectedBookDetails ;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Book Details" ;
    
    if (dictSelectedBookDetails) {
        lblBookID.text =[dictSelectedBookDetails valueForKey:@"Id"];
        lblBookTitle.text =[dictSelectedBookDetails valueForKey:@"Title"];
        lblBookAuthor.text =[dictSelectedBookDetails valueForKey:@"Author"];

    }
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
